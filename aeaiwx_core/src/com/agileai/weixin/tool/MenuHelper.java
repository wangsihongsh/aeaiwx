package com.agileai.weixin.tool;

import java.net.URLEncoder;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.weixin.model.Constans.Configs;
  
public class MenuHelper {
	private String appId = null;
    
	public MenuHelper(String appId){
		this.appId = appId;
	}
	
    public String createMenu(String params, String accessToken) throws Exception {   
    	CloseableHttpClient httpClient = null;
    	CloseableHttpResponse response = null;
		try {
	        httpClient = HttpClientManager.getSSLHttpClient();
			HttpPost httpost = HttpClientManager.getPostMethod("https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" + accessToken);    
	        httpost.setEntity(new StringEntity(params, "UTF-8"));    
	        response = httpClient.execute(httpost);    
	        String jsonStr = EntityUtils.toString(response.getEntity(), "utf-8");    
	        JSONObject demoJson = new JSONObject(jsonStr);  
	        return demoJson.getString("errmsg");  
		}finally{
    		if (response != null){
    			response.close();
    		}
    		if (httpClient != null){
    			httpClient.close();
    		}	
		}
    }    

    public String getMenuInfo(String accessToken) throws Exception {    
    	CloseableHttpClient httpClient = null;
    	CloseableHttpResponse response = null;
		try {
	        httpClient = HttpClientManager.getSSLHttpClient();
	        HttpGet get = HttpClientManager.getGetMethod("https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" + accessToken);    
	        response = httpClient.execute(get);    
	        String jsonStr = EntityUtils.toString(response.getEntity(), "utf-8");    
	        return jsonStr;  
		}finally{
    		if (response != null){
    			response.close();
    		}
    		if (httpClient != null){
    			httpClient.close();
    		}	
		}
    }    
  
    public String deleteMenuInfo(String accessToken) throws Exception {    
    	CloseableHttpClient httpClient = null;
    	CloseableHttpResponse response = null;
		try {
	        httpClient = HttpClientManager.getSSLHttpClient();
	        HttpGet get = HttpClientManager.getGetMethod("https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=" + accessToken);    
	        response = httpClient.execute(get);    
	        String jsonStr = EntityUtils.toString(response.getEntity(), "utf-8");    
	        JSONObject demoJson = new JSONObject(jsonStr);  
	        return demoJson.getString("errmsg");  
		}finally{
    		if (response != null){
    			response.close();
    		}
    		if (httpClient != null){
    			httpClient.close();
    		}	
		}
    }    
      
    private String buildViewURL(String redirectURL){
    	StringBuffer sb = new StringBuffer();
    	try {
    		redirectURL = URLEncoder.encode(redirectURL, "utf-8");			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	sb.append("https://open.weixin.qq.com/connect/oauth2/authorize?appid=").append(appId).append("&redirect_uri=").append(redirectURL).append("&response_type=code&scope=snsapi_base&state=1#wechat_redirect");
    	return sb.toString();
    }
    
    public static void main(String[] args) throws JSONException {
    	Configs.APPID = "Your AppID";
    	Configs.APPSECRET = "Your APPSECRET";
    	
    	String appId = Configs.APPID;
    	MenuHelper menuHelper = new MenuHelper(appId); 
    	
    	String ecpHomeURL = menuHelper.buildViewURL("http://www.agileai.com/portal/resource?WxRedirect&serviceId=home");
    	String ecpKmURL = menuHelper.buildViewURL("http://www.agileai.com/portal/resource?WxRedirect&serviceId=km");
    	String ecpBbsURL = menuHelper.buildViewURL("http://www.agileai.com/portal/resource?WxRedirect&serviceId=bbs");
    	
        StringBuffer sb = new StringBuffer();  
        sb.append("{");  
        sb.append(" \"button\":[");  
        sb.append("     {");  
        sb.append("         \"name\":\"移动办公\",");  
        sb.append("         \"type\":\"view\",");  
        sb.append("         \"url\":\"").append(ecpHomeURL).append("\"");  
        sb.append("     },");  
        sb.append("     {");  
        sb.append("         \"name\":\"知讯精选\",");  
        sb.append("         \"type\":\"view\",");  
        sb.append("         \"url\":\"").append(ecpKmURL).append("\"");  
        sb.append("     },");  
        sb.append("     {");  
        sb.append("         \"name\":\"合作交流\",");  
        sb.append("         \"sub_button\":[");  
        sb.append("             {");  
        sb.append("                 \"type\":\"view\",");  
        sb.append("                 \"name\":\"开发社区\",");  
        sb.append("                 \"url\":\"").append(ecpBbsURL).append("\"");  
        sb.append("             },");  
        sb.append("             {");  
        sb.append("                 \"type\":\"view\",");  
        sb.append("                 \"name\":\"手机网站\",");  
        sb.append("                 \"url\":\"http://www.agileai.com/portal/website/m1/index.ptml\"");  
        sb.append("             },");          
        sb.append("             {");  
        sb.append("                 \"type\":\"view\",");  
        sb.append("                 \"name\":\"关于我们\",");  
        sb.append("                 \"url\":\"http://www.agileai.com/portal/website/m1/content.ptml/others/Jtu1jNKL\"");  
        sb.append("             }");          
        sb.append("         ]");  
        sb.append("     }");         
        sb.append(" ]");  
        sb.append("}");  
        System.out.println(sb);
        try {  
            String result="";  
            
        	String accessToken = SecurityAuthHelper.getAccessToken();    
//          result = menuHelper.createMenu(sb.toString(), accessToken); 
          result = menuHelper.getMenuInfo(accessToken);
//            result = menuHelper.deleteMenuInfo(accessToken);
            System.out.println(result);    
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
  
    }  
  
}  